Name:    keyutils
Version: 1.6.3
Release: 5
Summary: Utilities of Linux Key Management
License: GPLv2+ and LGPLv2+

Url:     https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git
Source0: https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git/snapshot/keyutils-%{version}.tar.gz

Patch9000: bugfix-fix-argv-string-out-of-bounds.patch

BuildRequires: gcc gcc-c++
BuildRequires: glibc-kernheaders >= 2.4-9.1.92
Requires: %{name}-libs = %{version}-%{release}

%description
The keyutils contains the kernel key management facility and provides the
call back mechanism to get a key to user space.

%package libs
Summary: Key utilities library

%description libs
This package provides a wrapper library for the key management facility system
calls.

%package libs-devel
Summary: libs-devle package for keyutils
Requires: %{name}-libs = %{version}-%{release}

%description libs-devel
The libs-devel package of keyutils.

%package help
Summary: man files and other docs for keyutils

%description help
The help package for keyutils.

%prep
%autosetup -n %{name}-%{version} -p1

%build
make \
	NO_ARLIB=1 \
	ETCDIR=%{_sysconfdir} \
	LIBDIR=%{_libdir} \
	USRLIBDIR=%{_libdir} \
	BINDIR=%{_bindir} \
	SBINDIR=%{_sbindir} \
	MANDIR=%{_mandir} \
	INCLUDEDIR=%{_includedir} \
	SHAREDIR=%{_datarootdir}/keyutils \
	RELEASE=.%{release} \
	NO_GLIBC_KEYERR=1 \
	CFLAGS="-Wall $RPM_OPT_FLAGS" \
	LDFLAGS="%{?__global_ldflags}"

%install
make \
	NO_ARLIB=1 \
	DESTDIR=$RPM_BUILD_ROOT \
	ETCDIR=%{_sysconfdir} \
	LIBDIR=%{_libdir} \
	USRLIBDIR=%{_libdir} \
	BINDIR=%{_bindir} \
	SBINDIR=%{_sbindir} \
	MANDIR=%{_mandir} \
	INCLUDEDIR=%{_includedir} \
	SHAREDIR=%{_datarootdir}/keyutils \
	install

%post libs -p /sbin/ldconfig    

%postun libs -p /sbin/ldconfig

%files
%doc README
%license LICENCE.GPL
%license LICENCE.LGPL
%{_sbindir}/*
%{_bindir}/*
%{_datarootdir}/keyutils
%config(noreplace) %{_sysconfdir}/*

%files libs
%license LICENCE.LGPL
%{_libdir}/libkeyutils.so.*

%files libs-devel
%{_libdir}/libkeyutils.so
%{_includedir}/*
%{_libdir}/pkgconfig/libkeyutils.pc

%files help
%{_mandir}/man*/*

%changelog
* Fri Aug 05 2022 xuraoqing <xuraoqing@huawei.com> - 1.6.3-5
- DESC: fixed incorrect changelog time

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.6.3-4
- DESC: delete BuildRequires gdb

* Thu Jun 3 2021 steven <steven_ygui@163.com> - 1.6.3-3
- add build require of gcc-c++

* Mon Sep 14 2020 steven <steven_ygui@163.com> - 1.6.3-2
- update url and source0, and let it can be accessed

* Tue Jul 28 2020 steven <steven_ygui@163.com> - 1.6.3-1
- update to 1.6.3

* Thu Apr 16 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.6.1-1
- update to 1.6.1

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.5.10-11
- Add build requires of gdb

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.5.10-10
- add keyutils-libs containing dynamic library for keyutils

* Fri May 10 2019 huangchangyu <huangchangyu@huawei.com> - 1.5.10-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix argv string out of bounds

* Fri Mar 1 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.10-8
- Package init
